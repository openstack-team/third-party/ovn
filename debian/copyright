Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Open Virtual Network (OVN)
Source: https://github.com/ovn-org/ovn
Files-Excluded: debian

Files: *
Copyright: 2007-2017 Nicira, Inc.
           2010 Jean Tourrilhes - HP-Labs.
           2008,2009,2010 Citrix Systems, Inc.
           2011 Gaetano Catalli
           2000-2003 Geoffrey Wossum <gwossum@acm.org>
           2000 The NetBSD Foundation, Inc.
           1995, 1996, 1997, and 1998 WIDE Project.
           1982, 1986, 1990, 1993 The Regents of the University of California.
           2008, 2012 Vincent Bernat <bernat@luffy.cx>
           2014 Michael Chapman
           2014 WindRiver, Inc.
           2014 Avaya, Inc.
           2001 Daniel Hartmeier
           2002 - 2008 Henning Brauer
           2012 Gleb Smirnoff <glebius@FreeBSD.org>
           2015-2019 Red Hat, Inc.
           2017 DtDream Technology Co., Ltd.
           2018 eBay Inc.
License: Apache-2.0
Comment:
 This package contains code from other projects and authors not
 mentioned elsewhere.
 .
 Open vSwitch
 Copyright (c) 2007, 2008, 2009, 2010, 2011, 2013 Nicira, Inc.
 .
 Open vSwitch BSD port
 Copyright (c) 2011 Gaetano Catalli
 .
 Apache Portable Runtime
 Copyright 2008 The Apache Software Foundation.
 .
 This product includes software developed by
 The Apache Software Foundation (http://www.apache.org/).
 .
 Portions of this software were developed at the National Center
 for Supercomputing Applications (NCSA) at the University of
 Illinois at Urbana-Champaign.
 .
 lib/ovs.tmac includes troff macros written by Eric S. Raymond
 and Werner Lemberg.
 .
 m4/include_next.m4 and m4/absolute-header.m4
 Copyright (C) 2006-2013 Free Software Foundation, Inc.
 .
 Rapid Spanning Tree Protocol (RSTP) implementation
 Copyright (c) 2011-2014 M3S, Srl - Italy
 .
 LLDP implementation
 Copyright (c) 2008, 2012 Vincent Bernat <bernat@luffy.cx>
 .
 LLDP includes code used from the Net::CDP project based on the ISC license
 Copyright (c) 2014 Michael Chapman
 .
 LLDP includes code used from the ladvd project based on the ISC license
 Copyright (c) 2008, 2009, 2010 Sten Spans <sten@blinkenlights.nl>
 .
 Auto Attach implementation
 Copyright (c) 2014, 2015 WindRiver, Inc
 Copyright (c) 2014, 2015 Avaya, Inc
 .
 TCP connection tracker from FreeBSD pf, BSD licensed
 Copyright (c) 2001 Daniel Hartmeier
 Copyright (c) 2002 - 2008 Henning Brauer
 Copyright (c) 2012 Gleb Smirnoff <glebius@FreeBSD.org>

Files: build-aux/cccl
Copyright: 2000-2003 Geoffrey Wossum <gwossum@acm.org>
License: GPL-2

Files: utilities/bugtool/ovn-bugtool-*
       utilities/bugtool/plugins/network-status/ovn.xml
Copyright: 2016 Nicira, Inc.
License: LGPL-2.1

Files: m4/absolute-header.m4
       m4/include_next.m4
Copyright: Derek Price
           Paul Eggert
           2006-2013 Free Software Foundation, Inc.
License: file-is-free-software
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

Files: ovn-vif/*
Copyright: 2021 Canonical
License: Apache-2.0

License: GPL-2
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License v2
 (GPL) can be found in /usr/share/common-licenses/GPL-2.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License along
 with this library; if not, write to the Free Software Foundation, Inc., 51
 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 (LGPL) may be found in /usr/share/common-licenses/LGPL-2.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.
